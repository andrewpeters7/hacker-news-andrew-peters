import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
  base: '/',
  mode: 'history',
  routes: [
    {
      path: '/',
      component: () => import("@/views/home"),
    },
    {
      path: "*", //catch all
      component: () => import("@/views/not-found"),
    }
  ],
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
});

export default router;