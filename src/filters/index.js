export default {
    URLtoBaseURL(value) {
        if (typeof value !== 'string') {
            console.warn('Incorrect data passed to URLtoBaseURL', value);
            return '';
        }

        if (value.includes("//") === false) {
            console.warn('Invalid URL passed to URLtoBaseURL', value)
            return '';
        }

        return value.split("//")[1].split("/")[0];
    },
    dateTimeToAgo(value) {
        if (typeof value !== 'string') {
            console.warn('Incorrect data passed to dateTimeToAgo', value);
            return '';
        }

        const present = new Date(),
            date = new Date(value);

        //Not the most performant approach, but it's readable
        const difference = {
            year: present.getFullYear() - date.getFullYear(),
            month: present.getMonth() - date.getMonth(),
            day: present.getDate() - date.getDate(),
            hour: present.getHours() - date.getHours(),
            minute: present.getMinutes() - date.getMinutes()
        }

        for (const [key, dif] of Object.entries(difference)) {
            if (dif > 0) {
                return `${dif} ${key}${dif > 1 ? 's' : ''} ago`
            }
        }

        //We should only reach this if it was < 1 min ago.
        return 'moments ago'
    }
};