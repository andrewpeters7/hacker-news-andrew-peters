import Vue from 'vue'
import App from './App.vue'
import router from '@/router'

//Directives
import MaxEllipsis from "@/directives/max-ellipsis.js"

import Filters from '@/filters'

//Misc NPM Imports
import 'bootstrap/scss/bootstrap.scss';

import '@/scss/app.scss'

//Directives
Vue.directive('max-ellipsis', MaxEllipsis)

Object.entries(Filters).forEach(([key, filter]) => Vue.filter(key, filter))

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
