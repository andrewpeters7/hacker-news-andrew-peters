import axios from 'axios';

let call;
export default (url, config = {}) => {
    if (call !== undefined) {
        call.cancel("Only one request allowed at a time.");
    }
    call = axios.CancelToken.source();

    config.cancelToken = call.token
    return axios.get(url, config);
}