export default {
  bind(el, { value }) {
    if (value === undefined) {
      return console.warn('No bind value passed to max ellipsis.');
    }

    if (Number.isNaN(+value)) {
      return console.warn('Non-valid binding passed.');
    }

    el.style.maxWidth = `${value / 16}rem`;
    el.style.overflow = 'hidden';
    el.style.textOverflow = 'ellipsis';
    el.style.whiteSpace = 'nowrap';
    el.style.display = 'block';
    el.title = el.innerText;
  },
};
